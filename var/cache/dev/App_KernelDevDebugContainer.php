<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerF0XNFqJ\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerF0XNFqJ/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerF0XNFqJ.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerF0XNFqJ\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerF0XNFqJ\App_KernelDevDebugContainer([
    'container.build_hash' => 'F0XNFqJ',
    'container.build_id' => 'ca5c3529',
    'container.build_time' => 1646423106,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerF0XNFqJ');
