<?php

namespace App\Entity;

use App\Repository\ExpRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExpRepository::class)]
class Exp
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $specimen;

    // #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    // private $updatedAt;

    // #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    // private $createdAt;

    #[ORM\Column(type: 'integer')]
    private $temperature;

    #[ORM\Column(type: 'integer')]
    private $mesure;

    #[ORM\Column(type: 'integer')]
    private $time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecimen(): ?string
    {
        return $this->specimen;
    }

    public function setSpecimen(string $specimen): self
    {
        $this->specimen = $specimen;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTemperature(): ?int
    {
        return $this->temperature;
    }

    public function setTemperature(int $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getMesure(): ?int
    {
        return $this->mesure;
    }

    public function setMesure(int $mesure): self
    {
        $this->mesure = $mesure;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }
}
